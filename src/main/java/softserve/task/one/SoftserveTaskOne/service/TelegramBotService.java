package softserve.task.one.SoftserveTaskOne.service;

import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import softserve.task.one.SoftserveTaskOne.config.TelegramBotVariables;
import softserve.task.one.SoftserveTaskOne.model.Animal;
import softserve.task.one.SoftserveTaskOne.model.Breed;
import softserve.task.one.SoftserveTaskOne.model.Contact;
import softserve.task.one.SoftserveTaskOne.model.TelegramUser;
import softserve.task.one.SoftserveTaskOne.model.enums.Gender;
import softserve.task.one.SoftserveTaskOne.repository.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class TelegramBotService extends TelegramLongPollingBot {
    @Autowired
    private TelegramUserRepository telegramUserRepository;
    @Autowired
    private ShelterRepository shelterRepository;
    @Autowired
    private BreedRepository breedRepository;
    @Autowired
    private AnimalRepository animalRepository;
    @Autowired
    private PetRepository petRepository;
    @Autowired
    private ContactRepository contactRepository;
    private final TelegramBotVariables telegramBotVariables;
    private final List<BotCommand> commands = List.of(
            new BotCommand("/start", "get a welcome message"),
            new BotCommand("/menu", "display a menu")
    );
    static final String HELP_TEXT = "This bot is created to demonstrate Spring capabilities.\n\n" +
            "You can execute commands from the main menu on the left or by typing a command:\n\n" +
            "Type /start to see a welcome message\n\n" +
            "Type /mydata to see data stored about yourself\n\n" +
            "Type /help to see this message again";
    static final String FIND_FRIEND_BUTTON = "FIND_FRIEND_BUTTON";
    static final String FIND_HOME_BUTTON = "FIND_HOME_BUTTON";
    static final String CONTACT_BUTTON = "CONTACT_BUTTON";
    static final String SHELTER_BUTTON = "SHELTER";
    static final String FRIEND_BUTTON = "FRIEND";
    static final String ERROR_TEXT = "Error occurred: ";

    public TelegramBotService(TelegramBotVariables telegramBotVariables) {
        this.telegramBotVariables = telegramBotVariables;

        try {
            execute(new SetMyCommands(commands, new BotCommandScopeDefault(), null));
        } catch (TelegramApiException e) {
            log.error("Error setting bot's command list: " + e.getMessage());
        }
    }

    @Override
    public String getBotUsername() {
        return telegramBotVariables.getBotName();
    }

    @Override
    public String getBotToken() {
        return telegramBotVariables.getBotToken();
    }

    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();


            switch (messageText) {
                case "/start":
                case "start":
                    registerUser(update.getMessage());
                    startCommandReceived(chatId, update.getMessage().getChat().getFirstName());
                    break;
                case "/help":
                    prepareAndSendMessage(chatId, HELP_TEXT);
                    break;
                case "/menu":
                case "menu":
                    menuCommandReceiver(chatId);
                    break;
                default:
                    if (messageText.contains("/findfriend")) {
                        String[] arguments = messageText.split(" ");

                        try {
                            petRepository.findByNameContainingAndAnimalAndAgeAndGenderAndBreed(arguments[1],
                                    animalRepository.findByName(arguments[2]).orElseThrow(() -> new RuntimeException("Неіснуючий тип тварини")), Integer.valueOf(arguments[3]),
                                    Gender.valueOf(arguments[4]), breedRepository.findByName(arguments[5]).orElseThrow(() -> new RuntimeException("Неіснуюча порода"))).stream().forEach((pet -> {
                                SendMessage message = new SendMessage();
                                message.setChatId(String.valueOf(chatId));
                                message.setText(pet.getName());

                                InlineKeyboardButton friendButton = new InlineKeyboardButton();

                                friendButton.setText("Відгукнутися");
                                friendButton.setCallbackData(FRIEND_BUTTON + " " + pet.getId());

                                message.setReplyMarkup(new InlineKeyboardMarkup(List.of(List.of(friendButton))));

                                executeMessage(message);
                            }));
                        } catch (IndexOutOfBoundsException e) {
                            SendMessage message = new SendMessage();
                            message.setChatId(String.valueOf(chatId));
                            message.setText("Повинно бути 5 параметрів");

                            executeMessage(message);
                        } catch (RuntimeException e) {
                            SendMessage message = new SendMessage();
                            message.setChatId(String.valueOf(chatId));
                            message.setText(e.getMessage());

                            executeMessage(message);
                        }


                    } else if (messageText.contains("/findhome")) {

                    } else {
                        prepareAndSendMessage(chatId, "Sorry, command was not recognized");
                    }
            }

        } else if (update.hasCallbackQuery()) {
            String callbackData = update.getCallbackQuery().getData();
            long messageId = update.getCallbackQuery().getMessage().getMessageId();
            long chatId = update.getCallbackQuery().getMessage().getChatId();

            switch (callbackData) {
                case FIND_FRIEND_BUTTON:
                    executeEditMessageText("Для пошуку друга введіть наступну команду в заданому форматі: /findfriend ім'я тип_тварини вік стать порода", chatId, messageId);
                    break;
                case FIND_HOME_BUTTON:
                    executeEditMessageText("знайдено дім", chatId, messageId);
                    break;
                case CONTACT_BUTTON:
                    executeEditMessageText("Список притулків", chatId, messageId);

                    shelterRepository.findAll().stream().forEach((shelter -> {
                        SendMessage message = new SendMessage();
                        message.setChatId(String.valueOf(chatId));
                        message.setText(shelter.getName());

                        InlineKeyboardButton shelterButton = new InlineKeyboardButton();

                        shelterButton.setText("Детальніше");
                        shelterButton.setCallbackData(SHELTER_BUTTON + " " + shelter.getId());

                        message.setReplyMarkup(new InlineKeyboardMarkup(List.of(List.of(shelterButton))));

                        executeMessage(message);
                    }));

                    break;
                default:
                    if (callbackData.contains(SHELTER_BUTTON)) {
                        List<Contact> contacts = contactRepository.findByShelterId(Integer.valueOf(callbackData.split(" ")[1]));
                        StringBuilder message = new StringBuilder();

                        for (Contact contact : contacts) {
                            message.append(EmojiParser.parseToUnicode("Контактна особа " + ":memo:" + ": " + contact.getName() + "\n"));
                            message.append(EmojiParser.parseToUnicode("Телефон " + ":iphone:" + ": " + contact.getPhone() + "\n"));
                            message.append(EmojiParser.parseToUnicode("Адреса " + ":house:" + ": " + contact.getAddress() + "\n"));
                            message.append(EmojiParser.parseToUnicode("Сайт " + ":globe_with_meridians:" + ": " + contact.getUrl() + "\n\n"));
                        }

                        executeEditMessageText(message.toString(), chatId, messageId);
                    } else if (callbackData.contains(FRIEND_BUTTON)) {
                        executeEditMessageText("Отримайте", chatId, messageId);
                    }
            }
        }
    }

    private void menuCommandReceiver(long chatId) {

        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Зроби свій вибір!");

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        InlineKeyboardButton findFriendButton = new InlineKeyboardButton();

        findFriendButton.setText(EmojiParser.parseToUnicode("Знайти друга" + " :paw_prints:"));
        findFriendButton.setCallbackData(FIND_FRIEND_BUTTON);

        InlineKeyboardButton findHomeButton = new InlineKeyboardButton();

        findHomeButton.setText(EmojiParser.parseToUnicode("Знайти дім" + " :house:"));
        findHomeButton.setCallbackData(FIND_HOME_BUTTON);

        InlineKeyboardButton contactButton = new InlineKeyboardButton();

        contactButton.setText(EmojiParser.parseToUnicode("Контакти" + " :iphone:"));
        contactButton.setCallbackData(CONTACT_BUTTON);

        rowsInline.add(List.of(findFriendButton));
        rowsInline.add(List.of(findHomeButton));
        rowsInline.add(List.of(contactButton));

        markupInLine.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInLine);

        executeMessage(message);
    }

    private void registerUser(Message msg) {

        if (telegramUserRepository.findById(msg.getChatId()).isEmpty()) {

            var chatId = msg.getChatId();
            var chat = msg.getChat();

            TelegramUser telegramUser = new TelegramUser();

            telegramUser.setChatId(chatId);
            telegramUser.setFirstName(chat.getFirstName());
            telegramUser.setLastName(chat.getLastName());
            telegramUser.setUserName(chat.getUserName());

            telegramUserRepository.save(telegramUser);
            log.info("user saved: " + telegramUser);
        }
    }

    private void startCommandReceived(long chatId, String name) {


        String answer = EmojiParser.parseToUnicode("Hi, " + name + ", nice to meet you!" + " :blush:");
        log.info("Replied to user " + name);


        sendMessage(chatId, answer);
    }

    private void sendMessage(long chatId, String textToSend) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(textToSend);

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();

        List<KeyboardRow> keyboardRows = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();

        row.add("menu");
        row.add("start");

        keyboardRows.add(row);

        row = new KeyboardRow();

        row.add("menu");
        row.add("start");

        keyboardRows.add(row);

        keyboardMarkup.setKeyboard(keyboardRows);

        message.setReplyMarkup(keyboardMarkup);

        executeMessage(message);
    }


    private void executeEditMessageText(String text, long chatId, long messageId) {
        EditMessageText message = new EditMessageText();
        message.setChatId(String.valueOf(chatId));
        message.setText(text);
        message.setMessageId((int) messageId);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_TEXT + e.getMessage());
        }
    }

    private void executeEditMessageText(String text, long chatId, long messageId, InlineKeyboardMarkup markup) {
        EditMessageText message = new EditMessageText();
        message.setChatId(String.valueOf(chatId));
        message.setText(text);
        message.setMessageId((int) messageId);
        message.setReplyMarkup(markup);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_TEXT + e.getMessage());
        }
    }

    private void executeMessage(SendMessage message) {
        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_TEXT + e.getMessage());
        }
    }

    private void prepareAndSendMessage(long chatId, String textToSend) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(textToSend);
        executeMessage(message);
    }


}
