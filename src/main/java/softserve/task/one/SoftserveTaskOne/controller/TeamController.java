package softserve.task.one.SoftserveTaskOne.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import softserve.task.one.SoftserveTaskOne.repository.MemberRepository;

@Controller
@RequiredArgsConstructor
public class TeamController {
    private final MemberRepository memberRepository;
    @GetMapping("/teams")
    public String indexTeam(Model model) {
        model.addAttribute("members", memberRepository.findAll());
        return "team";
    }
}
