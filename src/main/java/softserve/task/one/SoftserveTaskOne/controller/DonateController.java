package softserve.task.one.SoftserveTaskOne.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import softserve.task.one.SoftserveTaskOne.model.Form;
import softserve.task.one.SoftserveTaskOne.repository.FormRepository;

@Controller
@RequiredArgsConstructor
public class DonateController {
    private final FormRepository formRepository;

    @GetMapping("/donates")
    public String indexDonate() {
        return "donate";
    }

    @PostMapping("/donates")
    public String addDonate(Form form) {
        formRepository.save(form);

        return "redirect:/donates";
    }
}
