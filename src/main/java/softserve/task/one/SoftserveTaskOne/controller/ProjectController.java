package softserve.task.one.SoftserveTaskOne.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import softserve.task.one.SoftserveTaskOne.repository.PostRepository;
import softserve.task.one.SoftserveTaskOne.repository.ProjectRepository;

@Controller
@RequiredArgsConstructor
public class ProjectController {
    private final ProjectRepository projectRepository;
    private final PostRepository postRepository;

    @GetMapping("/projects")
    public String indexProject(Model model) {
        model.addAttribute("projects", projectRepository.findAll());
        return "projects";
    }

    @GetMapping("/blogs")
    public String indexBlog(Model model) {
        model.addAttribute("posts", postRepository.findAll());
        return "blogs";
    }
}
