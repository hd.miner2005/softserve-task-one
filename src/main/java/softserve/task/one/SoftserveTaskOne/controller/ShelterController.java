package softserve.task.one.SoftserveTaskOne.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import softserve.task.one.SoftserveTaskOne.model.Shelter;
import softserve.task.one.SoftserveTaskOne.repository.ShelterRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Controller
@RequiredArgsConstructor
public class ShelterController {
    private final ShelterRepository shelterRepository;

    @GetMapping("/shelters")
    public String indexShelter(Model model) {
        model.addAttribute("shelters", shelterRepository.findAll());
        return "Shelters";
    }

    @GetMapping("/shelters/image/")
    public String imageShelter() {

        return "Shelters";
    }

    @PostMapping("/add/1")
    public String imageShe(@RequestParam MultipartFile file) throws IOException {
        Shelter sh = new Shelter();
        sh.setName("dffdf");
        sh.setImage(file.getBytes());
        shelterRepository.save(sh);
        return "redirect:/shelters";
    }
}
