package softserve.task.one.SoftserveTaskOne.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class ReportController {
    @GetMapping("/reports")
    public String indexReport() {
        return "reports";
    }
}
