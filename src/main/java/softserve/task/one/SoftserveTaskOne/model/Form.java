package softserve.task.one.SoftserveTaskOne.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 60)
    private String name;
    @Column(length = 60)
    private String surname;
    @Column(length = 60)
    private String email;
    @Column(length = 60)
    private String phone;
    @Column(length = 60)
    private String description;
}
