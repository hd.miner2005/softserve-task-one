package softserve.task.one.SoftserveTaskOne.model;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Type;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Shelter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 60)
    private String name;
    @Column(length = 1000)
    private String description;
    @Column(length = 40)
    private String city;
    @Column
    private Integer maxSize;
    @Column
    private Integer currentSize;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "shelter")
    private List<Contact> contacts = new ArrayList<>();
    @Lob
    private byte[] image;
}
