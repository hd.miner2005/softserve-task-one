package softserve.task.one.SoftserveTaskOne.model;

import jakarta.persistence.*;
import lombok.Data;
import softserve.task.one.SoftserveTaskOne.model.enums.Gender;

@Entity
@Data
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 30)
    private String name;
    @Column
    private int age;
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Animal animal;
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Breed breed;
    @Enumerated(EnumType.STRING)
    @Column
    private Gender gender;
    @Column
    private byte[] image;
}
