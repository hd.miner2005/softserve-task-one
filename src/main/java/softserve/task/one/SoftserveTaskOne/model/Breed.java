package softserve.task.one.SoftserveTaskOne.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Breed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 60)
    private String name;
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Animal animal;
}
