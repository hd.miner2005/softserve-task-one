package softserve.task.one.SoftserveTaskOne.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
@Entity
@Data
public class TelegramUser {
    @Id
    private Long chatId;

    private Long petId;

    private String firstName;

    private String lastName;

    private String userName;
}
