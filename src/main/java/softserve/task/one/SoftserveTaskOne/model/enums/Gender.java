package softserve.task.one.SoftserveTaskOne.model.enums;

public enum Gender {
    MALE, FEMALE
}
