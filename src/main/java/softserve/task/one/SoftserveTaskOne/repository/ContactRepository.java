package softserve.task.one.SoftserveTaskOne.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import softserve.task.one.SoftserveTaskOne.model.Contact;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
    List<Contact> findByShelterId(Integer shelterId);
}
