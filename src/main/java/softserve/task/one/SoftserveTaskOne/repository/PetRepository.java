package softserve.task.one.SoftserveTaskOne.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import softserve.task.one.SoftserveTaskOne.model.Animal;
import softserve.task.one.SoftserveTaskOne.model.Breed;
import softserve.task.one.SoftserveTaskOne.model.Pet;
import softserve.task.one.SoftserveTaskOne.model.enums.Gender;

import java.util.List;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {
    List<Pet> findByNameContainingAndAnimalAndAgeAndGenderAndBreed(String name, Animal animal, int age, Gender gender, Breed breed);
}
