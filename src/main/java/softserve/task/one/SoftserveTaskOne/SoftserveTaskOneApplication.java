package softserve.task.one.SoftserveTaskOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftserveTaskOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftserveTaskOneApplication.class, args);
	}

}
